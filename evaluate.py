import sys

count = 0
poscount = 0
with open(sys.argv[1]) as input_file:
    for line in input_file:
        line = line.strip()
        if line is "":
            continue
        args = line.split("\t")
        if args[0].strip() == args[1].strip():
            poscount += 1
        else:
            print(count)
        count += 1

print(float(poscount)/float(count))
