import sys
import csv
import re

with open(sys.argv[1]) as input_file:
    csvreader_file = csv.reader(input_file, delimiter=',')
    flflag = True
    lastSpeaker = ""
    csvreader = []
    for row in csvreader_file:
        csvreader.append(row)
    for i, row in enumerate(csvreader):
        if i is 0:
            continue
        crfline = ""

        if str(row[0]).strip() != "":
            crfline += str(row[0]) + "\t"
        else:
            crfline += "UNKN" + "\t"

        if flflag:
            crfline += "First_Utterance\t"

        speaker = str(row[1]).strip()
        if lastSpeaker != "":
            if speaker != lastSpeaker:
                crfline += "Speaker_Changed\t"

        lastSpeaker = speaker
        for arg in re.split("\\s+", row[2]):
            if arg == "":
                continue
            tokens = arg.split("/")
            crfline += "token_" + str(tokens[0]) + "\t"
            crfline += "pos_" + str(tokens[1]) + "\t"

        flflag = False

        print(crfline)
