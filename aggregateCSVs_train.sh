trainFile="all_usize_posbitri_expunc.train"
testFile="all_usize_posbitri_expunc.test"
rm $trainFile
rm $testFile
for file in `cat allTrainingFiles.shuffed.train`; do
	echo $file
	python3 csvPreprocess.py ./data/train/$file >> $trainFile
	echo "" >> $trainFile
done

for file in `cat allTrainingFiles.shuffed.test`; do
	echo $file
	python3 csvPreprocess.py ./data/train/$file >> $testFile
	echo "" >> $testFile
done
