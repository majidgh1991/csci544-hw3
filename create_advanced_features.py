import sys
import csv
import re

with open(sys.argv[1]) as input_file:
    csvreader_file = csv.reader(input_file, delimiter=',')
    flflag = True
    lastSpeaker = ""
    csvreader = []
    for row in csvreader_file:
        csvreader.append(row)
    for i, row in enumerate(csvreader):
        if i is 0:
            continue

        crfline = ""
        # row = [l.lower() for l in row]
        if str(row[0]).strip() != "":
            crfline += str(row[0]) + "\t"
        else:
            crfline += "UNKN" + "\t"

        if flflag:
            crfline += "First_Utterance\t"
        if i is len(csvreader)-1:
            crfline += "Last_Utterance\t"

        speaker = str(row[1]).strip()
        if lastSpeaker != "":
            if speaker != lastSpeaker:
                crfline += "Speaker_Changed\t"

        lastSpeaker = speaker
        words = []
        poss = []
        for arg in re.split("\\s+", row[2]):
            if arg == "":
                continue
            tokens = arg.split("/")
            words.append(str(tokens[0]))
            if str(tokens[1]) not in ".,?;":
                poss.append(str(tokens[1]))
            crfline += "token_" + str(tokens[0]) + "\t"
            crfline += "pos_" + str(tokens[1]) + "\t"
        if len(words) < 4:
            crfline += "Small_Sentence" + "\t"

        for j in range(len(poss)):
            if j < len(poss)-1: #bi
                crfline += poss[j] + "|" + poss[j+1] + "\t"
            if j < len(poss)-2: #tri
                crfline += poss[j] + "|" + poss[j+1] + "|" + poss[j+2] + "\t"
            # if j < len(poss)-3: #four
            #     crfline += poss[j] + "|" + poss[j+1] + "|" + poss[j+2] + "|" + poss[j+3] + "\t"


        flflag = False

        print(crfline)
